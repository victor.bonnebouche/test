package escape.from.tarkov.app.facades;

import escape.from.tarkov.app.service.HelloService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api("API for say hello")
public class HelloRestFacade {
  private static final String HELLO_PATH = "/hello";

  @Autowired
  HelloService helloService;


  @ApiOperation(value = "say hello")
  @RequestMapping(value = HELLO_PATH)
  public String hello() {
    return helloService.get();
  }

  @ApiOperation(value = "change hello")
  @PostMapping(value = HELLO_PATH + " new")
  public String changeHello(@RequestBody String newWorld) {
    return helloService.update(newWorld);
  }
}