package escape.from.tarkov.app.service.impl;

import escape.from.tarkov.app.service.HelloService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class HelloServiceImpl implements HelloService {

  String hello = "Soldier";

 public String update (String value){
    this.hello = value;
    return this.hello;
  }

  public String get (){
    return "Hello " + this.hello + " !";
  }
}