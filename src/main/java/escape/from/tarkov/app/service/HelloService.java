package escape.from.tarkov.app.service;

public interface HelloService {

  String update (String value);

  String get ();

}